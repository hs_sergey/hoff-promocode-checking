var ACTIVE_URL = "https://hoff.ru/personal/cart/";

var promocodesToTest = new Array();
var validPromocodes = new Array();
var invalidPromocodes = new Array();
var currentPromocodeIndex = 0;

var couponsToTest = new Array();
var validCoupons = new Array();
var invalidCoupons = new Array();
var currentCouponsIndex = 0;

function createPromocodeTestingButton() {
	var button = document.createElement("input");
	$(button).attr("type", "button");
	$(button).attr("id", "btnAddPromocodesToTest");
	$(button).attr("style", "margin: 20px; padding: 5px;");
	$(button).val("Добавить промокоды для тестирования...");
	$("#checkPromocodesContainer").get(0).appendChild(button);	
}

function createAddPromocodesDialog() {
	var div = document.createElement("div");
	$(div).attr("id", "addPromocodesDialog");
	$(div).attr("style", "display: none;");
	var table = document.createElement("table");
	var tr = document.createElement("tr");
	table.appendChild(tr);
	var td = document.createElement("td");
	$(td).html("Промокоды");
	tr.appendChild(td);
	td = document.createElement("td");
	$(td).html("Купоны");
	tr.appendChild(td);
	tr = document.createElement("tr");
	table.appendChild(tr);
	td = document.createElement("td");
	tr.appendChild(td);
	var textarea = document.createElement("textarea");
	$(textarea).attr("id", "addingPromocodes");
	$(textarea).attr("style", "width: 200px; height: 500px;");
	$(textarea).attr("placeholder", "Введите промокоды, по одному в строчке");
	td.appendChild(textarea);
	td = document.createElement("td");
	tr.appendChild(td);
	textarea = document.createElement("textarea");
	$(textarea).attr("id", "addingCoupons");
	$(textarea).attr("style", "width: 200px; height: 500px;");
	$(textarea).attr("placeholder", "Введите купоны, по одному в строчке");
	td.appendChild(textarea);
	tr = document.createElement("tr");
	table.appendChild(tr);
	td = document.createElement("td");
	tr.appendChild(td);	
	var button = document.createElement("input");
	$(button).attr("type", "button");
	$(button).attr("id", "btnStartTest");
	$(button).attr("style", "margin: 10px; padding: 5px;");
	$(button).val("Запуск тестирования");
	td.appendChild(button);
	td = document.createElement("td");
	tr.appendChild(td);	
	button = document.createElement("input");
	$(button).attr("type", "button");
	$(button).attr("id", "btnStartTestCoupons");
	$(button).attr("style", "margin: 10px; padding: 5px;");
	$(button).val("Запуск тестирования");
	td.appendChild(button);
	div.appendChild(table);
	$("#checkPromocodesContainer").get(0).appendChild(div);
}

function startPromocodeTesting() {
	$("#checkPromocodesContainer").html("");
	console.log("startPromocodeTesting");
	currentPromocodeIndex = 0;
	validPromocodes = new Array();
	localStorage.setItem("validPromocodes", implode("\n", validPromocodes));
	invalidPromocodes = new Array();
	localStorage.setItem("invalidPromocodes", implode("\n", invalidPromocodes));
	localStorage.setItem("state", "runningPromocodes");
	localStorage.setItem("currentPromocodeIndex", currentPromocodeIndex);
	localStorage.setItem("promocodesToTest", implode("\n", promocodesToTest));

	checkPromocode1();
	
//	$(".js-voucher-set")[0].click();
//	setTimeout(checkPromocode1, 500);
}

function startCouponTesting() {
	$("#checkPromocodesContainer").html("");
	console.log("startCouponTesting");
	currentCouponIndex = 0;
	validCoupons = new Array();
	localStorage.setItem("validCoupons", implode("\n", validCoupons));
	invalidCoupons = new Array();
	localStorage.setItem("invalidCoupons", implode("\n", invalidCoupons));
	localStorage.setItem("state", "runningCoupons");
	localStorage.setItem("currentCouponIndex", currentCouponIndex);
	localStorage.setItem("couponsToTest", implode("\n", couponsToTest));

	checkCoupon1();
	
//	$(".js-voucher-set")[0].click();
//	setTimeout(checkPromocode1, 500);
}

function checkPromocode1() {
	if(window.location.href.indexOf(ACTIVE_URL) === -1) {
		console.log("exiting case url changed");
		return;
	}
	localStorage.setItem("state", "runningPromocodes");
	console.log("checkPromocode1");
	console.log("cheking promocode " + (currentPromocodeIndex+1)  + " of " + promocodesToTest.length);
	$('input[name="ORDER_PROP_25"]').val(promocodesToTest[currentPromocodeIndex]);
	setTimeout(checkPromocode2, 1000);
}

function checkPromocode2() {
	if(window.location.href.indexOf(ACTIVE_URL) === -1) {
		console.log("exiting case url changed");
		return;
	}
	console.log("checkPromocode2");
//	localStorage.setItem("state", "checkPromocode2");
	$('input[name="ORDER_PROP_25"]').parent().find(".elem-btn")[0].click();
	setTimeout(checkPromocode3, 5000);
}

function checkPromocode3() {
	if(window.location.href.indexOf(ACTIVE_URL) === -1) {
		console.log("exiting case url changed");
		return;
	}
	console.log("checkPromocode3");
	var foundDiscount = $('input[name="ORDER_PROP_25"]').parent().find(".success").is(':visible');
	if(foundDiscount) {
		console.log("promocode " + promocodesToTest[currentPromocodeIndex] + " is VALID");
		validPromocodes.push(promocodesToTest[currentPromocodeIndex]);
		localStorage.setItem("validPromocodes", implode("\n", validPromocodes));
		currentPromocodeIndex++;
		localStorage.setItem("currentPromocodeIndex", currentPromocodeIndex);
		if(currentPromocodeIndex >= promocodesToTest.length) {
			localStorage.setItem("state", "finished");
			handleAllPromocodesChecked();
		} else {
//			localStorage.setItem("state", "checkPromocode3");
			setTimeout(checkPromocode1, 1000);	
		}
	} else {
		console.log("promocode " + promocodesToTest[currentPromocodeIndex] + " is INVALID");
		invalidPromocodes.push(promocodesToTest[currentPromocodeIndex]);
		localStorage.setItem("invalidPromocodes", implode("\n", invalidPromocodes));
		currentPromocodeIndex++;
		localStorage.setItem("currentPromocodeIndex", currentPromocodeIndex);
		if(currentPromocodeIndex >= promocodesToTest.length) {
			localStorage.setItem("state", "finished");
			handleAllPromocodesChecked();
		} else {
			setTimeout(checkPromocode1, 1000);	
		}
	}
}

function checkCoupon1() {
	if(window.location.href.indexOf(ACTIVE_URL) === -1) {
		console.log("exiting case url changed");
		return;
	}
	localStorage.setItem("state", "runningCoupons");
	console.log("checkCoupon1");
	console.log("cheking coupon " + (currentCouponIndex+1)  + " of " + couponsToTest.length);
	$('input[name="ORDER_PROP_33"]').val(couponsToTest[currentCouponIndex]);
	$('input[name="ORDER_PROP_33"]')[0].click();
	setTimeout(checkCoupon2, 1000);
}

function checkCoupon2() {
	if(window.location.href.indexOf(ACTIVE_URL) === -1) {
		console.log("exiting case url changed");
		return;
	}
	console.log("checkCoupon2");
//	localStorage.setItem("state", "checkPromocode2");
	$('input[name="ORDER_PROP_33"]').parent().find(".elem-btn")[0].click();
	setTimeout(checkCoupon3, 5000);
}

function checkCoupon3() {
	if(window.location.href.indexOf(ACTIVE_URL) === -1) {
		console.log("exiting case url changed");
		return;
	}
	console.log("checkCoupon3");
	var foundDiscount = $('input[name="ORDER_PROP_33"]').parent().find(".success").is(':visible');
	if(foundDiscount) {
		console.log("coupon " + couponsToTest[currentCouponIndex] + " is VALID");
		validCoupons.push(couponsToTest[currentCouponIndex]);
		localStorage.setItem("validCoupons", implode("\n", validCoupons));
		currentCouponIndex++;
		localStorage.setItem("currentCouponIndex", currentCouponIndex);
		if(currentCouponIndex >= couponsToTest.length) {
			localStorage.setItem("state", "finished");
			handleAllPromocodesChecked();
		} else {
//			localStorage.setItem("state", "checkPromocode3");
			setTimeout(checkCoupon1, 1000);	
		}
	} else {
		console.log("coupon " + couponsToTest[currentCouponIndex] + " is INVALID");
		invalidCoupons.push(couponsToTest[currentCouponIndex]);
		localStorage.setItem("invalidCoupons", implode("\n", invalidCoupons));
		currentCouponIndex++;
		localStorage.setItem("currentCouponIndex", currentCouponIndex);
		if(currentCouponIndex >= couponsToTest.length) {
			localStorage.setItem("state", "finished");
			handleAllPromocodesChecked();
		} else {
			setTimeout(checkCoupon1, 1000);	
		}
	}
}


function handleAllPromocodesChecked() {
	if(window.location.href.indexOf(ACTIVE_URL) === -1) {
		console.log("exiting case url changed");
		return;
	}
	console.log("all promocodes are checked");
	var div = null;
	if($("#checkPromocodesContainer").length) {
		div = $("#checkPromocodesContainer").get(0);
		$(div).html("");
	} else {
		div = document.createElement("div");
		$(div).attr("id", "checkPromocodesContainer");
		$(div).attr("style", "margin: 20px; padding: 10px; border: solid 1px blue; overflow: hidden; width: 100%;");
	}
	var table = document.createElement("table");
	div.appendChild(table);
	var tr = document.createElement("tr");
	table.appendChild(tr);
	var td = document.createElement("td");
	tr.appendChild(td);
	
	var label = document.createElement("label");
	$(label).attr("style", "margin-top: 20px; display: block;");
	$(label).html("Валидные промокоды:");
	td.appendChild(label);
	var textarea = document.createElement("textarea");
	$(textarea).attr("style", "width: 200px; height: 500px;");
	$(textarea).html(implode("\n", validPromocodes));
	td.appendChild(textarea);
	label = document.createElement("label");
	$(label).attr("style", "margin-top: 20px; display: block;");
	$(label).html("Невалидные промокоды:");
	td.appendChild(label);
	textarea = document.createElement("textarea");
	$(textarea).attr("style", "width: 200px; height: 500px;");
	$(textarea).html(implode("\n", invalidPromocodes));
	td.appendChild(textarea);
	var button = document.createElement("input");
	$(button).attr("type", "button");
	$(button).attr("id", "btnNewTest");
	$(button).attr("style", "margin: 10px; padding: 5px;");
	$(button).val("Начать заново");
	td.appendChild(button);
	
	td = document.createElement("td");
	tr.appendChild(td);
	
	var label = document.createElement("label");
	$(label).attr("style", "margin-top: 20px; display: block;");
	$(label).html("Валидные купоны:");
	td.appendChild(label);
	var textarea = document.createElement("textarea");
	$(textarea).attr("style", "width: 200px; height: 500px;");
	$(textarea).html(implode("\n", validCoupons));
	td.appendChild(textarea);
	label = document.createElement("label");
	$(label).attr("style", "margin-top: 20px; display: block;");
	$(label).html("Невалидные купоны:");
	td.appendChild(label);
	textarea = document.createElement("textarea");
	$(textarea).attr("style", "width: 200px; height: 500px;");
	$(textarea).html(implode("\n", invalidCoupons));
	td.appendChild(textarea);
	var button = document.createElement("input");
	$(button).attr("type", "button");
	$(button).attr("id", "btnNewTestCoupons");
	$(button).attr("style", "margin: 10px; padding: 5px;");
	$(button).val("Начать заново");
	td.appendChild(button);
	
	
	$(".box-footer__contacts").get(0).appendChild(div);
	
	$("#btnNewTest").click(function() {
		startNewTest();
	});
	$("#btnNewTestCoupons").click(function() {
		startNewTest();
	});

}

function implode( glue, pieces ) {	// Join array elements with a string
	// 
	// +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// +   improved by: _argos
	return ( ( pieces instanceof Array ) ? pieces.join ( glue ) : pieces );
}

function startNewTest() {
	$("#checkPromocodesContainer").html("");
	createAddPromocodesDialog();
//	createPromocodeTestingButton();
	$("#addPromocodesDialog").show();
//	$("#btnAddPromocodesToTest").hide();
	
//	$("#btnAddPromocodesToTest").click(function() {
//		console.log("add promocodes to test click");
//		$("#addPromocodesDialog").show();
//		$("#btnAddPromocodesToTest").hide();
//	});
	
	$("#btnStartTest").click(function() {
		var promocodes = $("#addingPromocodes").val();
		if(promocodes) {
			promocodesToTest = promocodes.split("\n");
			console.log("found " + promocodesToTest.length + " promocodes to test");
			startPromocodeTesting();
		}
	});

	$("#btnStartTestCoupons").click(function() {
		var coupons = $("#addingCoupons").val();
		if(coupons) {
			couponsToTest = coupons.split("\n");
			console.log("found " + couponsToTest.length + " coupons to test");
			startCouponTesting();
		}
	});
	
}

$(document).ready(function() {
	console.log("content js document ready");
	var savedState = localStorage.getItem("state");
	currentPromocodeIndex = parseInt(localStorage.getItem("currentPromocodeIndex"));
	var savedPromocodesToTest = localStorage.getItem("promocodesToTest");
	if(savedPromocodesToTest) {
		promocodesToTest = savedPromocodesToTest.split("\n");
	}
	var savedValidPromocodes = localStorage.getItem("validPromocodes");
	if(savedValidPromocodes) {
		validPromocodes = savedValidPromocodes.split("\n");
	}
	var savedInvalidPromocodes = localStorage.getItem("invalidPromocodes");
	if(savedInvalidPromocodes) {
		invalidPromocodes = savedInvalidPromocodes.split("\n");
	}
	currentCouponIndex = parseInt(localStorage.getItem("currentCouponIndex"));
	var savedCouponsToTest = localStorage.getItem("couponsToTest");
	if(savedCouponsToTest) {
		couponsToTest = savedCouponsToTest.split("\n");
	}
	var savedValidCoupons = localStorage.getItem("validCoupons");
	if(savedValidCoupons) {
		validCoupons = savedValidCoupons.split("\n");
	}
	var savedInvalidCoupons = localStorage.getItem("invalidCoupons");
	if(savedInvalidCoupons) {
		invalidCoupons = savedInvalidCoupons.split("\n");
	}
	if(savedState == "finished") {
		handleAllPromocodesChecked();
//	} else if(savedState == "checkPromocode2") {
//		localStorage.setItem("state", "checkPromocode2-refreshing");
//		window.location = "https://www.ricaud.com/ru-ru/zakaz/?rv=" + Math.floor(Math.random() * 100);
//	} else if(savedState == "checkPromocode2-refreshing") {
//		checkPromocode3();
//	} else if(savedState == "checkPromocode3") {
//		checkPromocode1();
////		$(".js-voucher-set")[0].click();
////		setTimeout(checkPromocode1, 500);
	} else if(savedState == "runningPromocodes") {
		var div = document.createElement("div");
		$(div).attr("id", "checkPromocodesContainer");
		$(div).attr("style", "margin: 20px; padding: 10px; border: solid 1px blue; overflow: hidden; width: 100%;");
		$(".box-footer__contacts").get(0).appendChild(div);
		
		var label = document.createElement("label");
		$(label).html("Предыдущая проверка была прервана! Проверено " + currentPromocodeIndex + " промокодов из " + promocodesToTest.length);
		$("#checkPromocodesContainer").get(0).appendChild(label);
		
		var button = document.createElement("input");
		$(button).attr("type", "button");
		$(button).attr("id", "btnContinueTest");
		$(button).attr("style", "margin: 10px; padding: 5px;");
		$(button).val("Продолжить проверку промокодов");
		$("#checkPromocodesContainer").get(0).appendChild(button);
		
		button = document.createElement("input");
		$(button).attr("type", "button");
		$(button).attr("id", "btnNewTest");
		$(button).attr("style", "margin: 10px; padding: 5px;");
		$(button).val("Начать заново");
		$("#checkPromocodesContainer").get(0).appendChild(button);
		
		$("#btnContinueTest").click(function() {
			$("#checkPromocodesContainer").html("");
			$("label[for='promocode']").click();
			setTimeout(checkPromocode1, 3000);
		});

		$("#btnNewTest").click(function() {
			startNewTest();
		});
	} else if(savedState == "runningCoupons") {
		var div = document.createElement("div");
		$(div).attr("id", "checkPromocodesContainer");
		$(div).attr("style", "margin: 20px; padding: 10px; border: solid 1px blue; overflow: hidden; width: 100%;");
		$(".box-footer__contacts").get(0).appendChild(div);
		
		var label = document.createElement("label");
		$(label).html("Предыдущая проверка была прервана! Проверено " + currentCouponIndex + " купонов из " + couponsToTest.length);
		$("#checkPromocodesContainer").get(0).appendChild(label);
		
		var button = document.createElement("input");
		$(button).attr("type", "button");
		$(button).attr("id", "btnContinueTestCoupons");
		$(button).attr("style", "margin: 10px; padding: 5px;");
		$(button).val("Продолжить проверку купонов");
		$("#checkPromocodesContainer").get(0).appendChild(button);
		
		button = document.createElement("input");
		$(button).attr("type", "button");
		$(button).attr("id", "btnNewTest");
		$(button).attr("style", "margin: 10px; padding: 5px;");
		$(button).val("Начать заново");
		$("#checkPromocodesContainer").get(0).appendChild(button);
		
		$("#btnContinueTestCoupons").click(function() {
			$("#checkPromocodesContainer").html("");
			$("label[for='promocode']").click();
			setTimeout(checkCoupon1, 3000);
		});

		$("#btnNewTest").click(function() {
			startNewTest();
		});
	} else {
		var div = document.createElement("div");
		$(div).attr("id", "checkPromocodesContainer");
		$(div).attr("style", "margin: 20px; padding: 10px; border: solid 1px blue; overflow: hidden; width: 100%;");
		$(".box-footer__contacts").get(0).appendChild(div);
		startNewTest();
	}
	
});
